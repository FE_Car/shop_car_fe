import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PromotionComponent } from "./features/components/promotion/promotion.component";
import { AccountManagementComponent } from "./features/components/account-management/account-management.component";
import { OrderManagementComponent } from "./features/components/order-management/order-management.component";
import { LayoutModule } from "./features/layout/layout.module";
import { DefaultLayoutModule } from "./layouts/default-layout/default-layout.module";
import { OnlyLayoutModule } from "./layouts/only-layout/only-layout.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ProductComponent } from "./features/components/product/product.component";
import { DashboardComponent } from './features/components/dashboard/dashboard.component';
import { BrandCarComponent } from './features/components/brand-car/brand-car.component';
import { FormEditProductComponent } from './features/components/form-edit-product/form-edit-product.component';
import { FormCreateProductComponent } from './features/components/form-create-product/form-create-product.component';
import { ServicesComponent } from './features/components/services/services.component';
import { ShowRoomComponent } from './features/components/show-room/show-room.component';

@NgModule({
  declarations: [
    AppComponent,
    PromotionComponent,
    AccountManagementComponent,
    OrderManagementComponent,
    ProductComponent,
    DashboardComponent,
    BrandCarComponent,
    FormEditProductComponent,
    FormCreateProductComponent,
    ServicesComponent,
    ShowRoomComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    DefaultLayoutModule,
    OnlyLayoutModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
