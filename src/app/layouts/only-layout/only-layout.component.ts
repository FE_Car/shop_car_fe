import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-only-layout',
  templateUrl: './only-layout.component.html',
  styleUrls: ['./only-layout.component.css']
})
export class OnlyLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
