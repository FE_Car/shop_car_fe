import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlyLayoutComponent } from './only-layout.component';

describe('OnlyLayoutComponent', () => {
  let component: OnlyLayoutComponent;
  let fixture: ComponentFixture<OnlyLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlyLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlyLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
