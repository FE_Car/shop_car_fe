import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnlyLayoutComponent } from './only-layout.component';
import { LoginComponent } from 'src/app/features/components/login/login.component';
import { PageNotFoundComponent } from 'src/app/features/components/page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';
import { LayoutModule } from 'src/app/features/layout/layout.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [OnlyLayoutComponent, LoginComponent, PageNotFoundComponent],
  imports: [
    CommonModule, RouterModule, LayoutModule, FormsModule
  ]
})
export class OnlyLayoutModule { }
