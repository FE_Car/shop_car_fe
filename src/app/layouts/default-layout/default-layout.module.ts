import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultLayoutComponent } from './default-layout.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { LayoutModule } from 'src/app/features/layout/layout.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [DefaultLayoutComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    LayoutModule,
    FormsModule
  ]
})
export class DefaultLayoutModule { }
