import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from './layouts/default-layout/default-layout.component';
import { ProductComponent } from './features/components/product/product.component';
import { PageNotFoundComponent } from './features/components/page-not-found/page-not-found.component';
import { AccountManagementComponent } from './features/components/account-management/account-management.component';
import { OnlyLayoutComponent } from './layouts/only-layout/only-layout.component';
import { LoginComponent } from './features/components/login/login.component';
import { OrderManagementComponent } from './features/components/order-management/order-management.component';
import { PromotionComponent } from './features/components/promotion/promotion.component';
import { DashboardComponent } from './features/components/dashboard/dashboard.component';
import { BrandCarComponent } from './features/components/brand-car/brand-car.component';
import { FormEditProductComponent } from './features/components/form-edit-product/form-edit-product.component';
import { FormCreateProductComponent } from './features/components/form-create-product/form-create-product.component';
import { ServicesComponent } from './features/components/services/services.component';
import { ShowRoomComponent } from './features/components/show-room/show-room.component';


const routes: Routes = [
  { path: '', redirectTo: '/my-dashboard-car/dashboard', pathMatch: 'full' },
{
  path: 'auth',
  component: OnlyLayoutComponent,
  children: [
    {
      path: 'login',
      component: LoginComponent
    }
  ]
},
{
  path: 'my-dashboard-car',
  component: DefaultLayoutComponent,
  children: [
    {path:'dashboard', component: DashboardComponent},
    {path:'account-management', component: AccountManagementComponent},
    {path:'order-management', component: OrderManagementComponent},
    {path:'promotion', component: PromotionComponent},
    {path:'services', component: ServicesComponent},
    {path: "barnd-car", component: BrandCarComponent},
    {path:'product', component: ProductComponent},
    {path: "form-edit-product", component: FormEditProductComponent},
    {path: "form-create-product", component: FormCreateProductComponent},
    {path: "show-room", component: ShowRoomComponent},
  ]
},
{ path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
